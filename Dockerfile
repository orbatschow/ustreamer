FROM debian:stable-20200720-slim as builder

RUN apt-get update && apt-get install -y \
      build-essential \
      libevent-dev \
      libjpeg62-turbo-dev \
      uuid-dev \
      libbsd-dev

WORKDIR /ustreamer

COPY . .

RUN make

FROM debian:stable-20200720-slim

RUN apt-get update && apt-get install -y \
      build-essential \
      libevent-dev \
      libjpeg62-turbo-dev \
      uuid-dev \
      libbsd-dev

COPY --from=builder /ustreamer/ustreamer /usr/local/bin

CMD ["ustreamer"]
